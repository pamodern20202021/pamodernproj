#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>
#include<array>
#include<queue>
enum class AudioFileFormat
{
	Error,
	NotLoaded,
	WAV
};


class AudioFile
{
public:
	typedef std::vector < std::vector<int16_t> >AudioBuffer;

	//Constructor
	AudioFile();

	//Constructor with a given path

	AudioFile(std::string filePath);

	//Load audio file from given path
	//returns true if successfully loaded

	bool load(std::string filePath);

	//Save the audio file to a given path 
	//return true if successfully saved

	bool save(const std::string& filePath, AudioFileFormat = AudioFileFormat::WAV);
	

	AudioFileFormat audioFileFormat;
	
	
	AudioFileFormat	determineAudioFileFormat(std::string filename);
	void AddToStorage( std::queue<int8_t>&fullData,std::array<int16_t,4>&datain);
	void AddToStorage( std::queue<int8_t>&fullData,std::array<int16_t,2>&datain);
	void AddToStorage( std::queue<int8_t>&fullData,std::vector<int16_t>&datain);
	void AddToQueue( std::queue<int16_t>&fullData,std::array<int16_t,4>&datain);
	void AddToQueue( std::queue<int16_t>&fullData,std::array<int16_t,2>&datain);
	void AddToQueue( std::queue<int16_t>&fullData,std::vector<int16_t>&datain);

	public:
	std::array<int16_t,4>m_riffChunkId,m_riffChunkSize,m_riffFormat;
	std::array<int16_t,4>m_fmtsChunckId,m_fmtsChunkSize;
	std::array<int16_t,2>m_AudioFormat,m_numChannels;
	std::array<int16_t,4>m_sampleRate,m_byterate;
	std::array<int16_t,2>m_blockAlign,m_bitsPerSample;
	std::array<int16_t,4>m_datasChunckId,m_datasChunkSize;
	std::vector<int16_t> m_data;
};
