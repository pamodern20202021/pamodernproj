#pragma once
#include "AudioFile.h"
#include <string>
#include <fstream>
#include<iostream>


class ConfigFile
{
public:
	// struct for scaling and playback
	struct factortype 
	{
		int16_t factor;
		std::string type;

	};

	/*AudioFile<int16_t> Input();*/
	void Output();

	// constructors
	ConfigFile();
	ConfigFile(const std::string& fileName);
	
	//setters
	static void SetFactorType(const std::string& data, factortype& scaleorplayback);

	//getters
	factortype& GetScale();
	factortype& GetPlayback();
	std::string GetInputData() const;
	std::string GetOutputData() const;
	std::string GetProcessors() const;
	factortype  m_scale;
	factortype m_playback;
private:
	std::string m_inputData;
	std::string m_outputData;
	std::string m_processors;

};

