#pragma once
#include<vector>
#include"scaling.h"
class playbackSpeed
{
public:
	std::vector<int16_t> increaseSpeed(int16_t percentage, std::vector<int16_t> sounds);
	std::vector<int16_t> decreaseSpeed(int16_t percentage, std::vector<int16_t> sounds);
	
private:
	int16_t m_playbackSpeed;
};

