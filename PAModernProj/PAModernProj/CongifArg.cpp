#include "ConfigArg.h"

ConfigArg::ConfigArg(int argc, char** argv)
{

	m_args = std::vector<std::string>(argv + 1, argv + argc);
	if (argc != 1) {
		std::cout << "\n<---" << m_args.size() << " Arguments Loaded--->\n";
	}
	else
		std::cout << "\n<---No Arguments Loaded--->\n";

}
ConfigArg::~ConfigArg()
{

}

bool ConfigArg::ProcessArg(const int16_t& argNo, bool haveParam)
{
	if (m_args.size() < argNo)
	{
		std::cout << "\n<---Arg doesn't exist--->\n";
		return false;
	}

	std::string argument = m_args[argNo - 1];
	std::string argparam;
	std::cout << argument;
	if (argument[0] == '-')
	{
		if (haveParam)
		{
			if (m_args.size() >= argNo + 1) {
				argparam = m_args[argNo];
				if (argparam[0] == '-')
				{
					std::cout << "\n<---This Arg doesn't have a parameter (it needs one)--->\n";
					return false;
				}
			}
			else
			{

				std::cout << "\n<---This Arg doesn't have a parameter (it needs one)--->\n";
				return false;
			}
		}
		else
		{
			// run the task
		}



	}
	else
	{
		std::cout << "\n<---Posibly the argument is a param--->\n";
		return false;
	}

}
bool ConfigArg::RunByArg(std::string arg, std::string param)
{

}

void ConfigArg::x(std::string chosenOption)
{
	int option = 0;
	if (chosenOption == "--help")
		option = 1;
	if (chosenOption == "--process in")
		option = 2;
	if (chosenOption == "--process out")
		option = 3;
	if (chosenOption == "--checkstatus")
		option = 4;
	if (chosenOption == "--playback")
		option = 5;
	if (chosenOption == "--scale")
		option = 6;
	if (chosenOption == "==generatesynus")
		option = 7;


	switch (option)
	{
	case 1: // --help case
	{
		std::cout << std::endl << std::endl;
		std::cout << "<---- HELP ---->" << std::endl;
		std::cout << "<== --process in <filetype> <path>  ==> import audio file from a given path" << std::endl;
		std::cout << "<== --process out <filetype> ==> export audio file" << std::endl;
		std::cout << "<== --checkstatus ==> shows information about the imported file" << std::endl;
		std::cout << "<== --playback <percentage> ==> choose a playback speed between 0.5x to 2x" << std::endl;
		std::cout << "<== --scale <low,up> <percentage> ==> lower/upper scale the audio signal" << std::endl;
		std::cout << "<== --generatesynus ==> generate the synusoidal form" << std::endl;
		std::cout << "<---- HELP ---->" << std::endl << std::endl;
		break;
	}
	case 2: // --process in case
	{

	}
	case 3: // --process out case
	{

	}
	case 4: // --checkstatus case
	{

	}
	case 5: // --playback case
	{

	}
	case 6: // --scale case
	{

	}
	case 7: // --generatesynus case
	{

	}
	}
}