#include "sinusoidalWave.h"
#define NUMBER_OF_SAMPLES 100
#define OFFSET        3276
#define AMPLITUDE     32767
#define M_PI          3.14159265359

void sinusoidalWave::setDigitalSinusWave(std::vector<int16_t>& sounds)
{
	double_t angle = 0.0;
	
	for (int16_t sound : sounds)
	{
		sound = int16_t(AMPLITUDE * sin(angle) + OFFSET);
		angle += (2 * M_PI) / NUMBER_OF_SAMPLES;
	}
	
}

std::vector<int16_t> sinusoidalWave::getDigitalSunusWave(std::vector<int16_t>& sounds)
{
	setDigitalSinusWave(sounds);
	return sounds;
}
