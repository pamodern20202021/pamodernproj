#include "ConfigFile.h"

ConfigFile::ConfigFile()
{
	std::ifstream in("CFG.txt");
	std::string aux;
	while (!in.eof())
	{
		in >> aux;
		if (aux == "[Input]")
		{
			in >> aux;
			m_inputData = aux;
		}

		if (aux == "[Output]")
		{
			in >> aux;
			m_outputData = aux;
		}

		if (aux == "[Processors]")
		{
			in >> aux;
			m_processors = aux;
		}

		if (aux == "[scale]")
		{
			in >> aux;
			SetFactorType(aux,m_scale);
			in >> aux;
			SetFactorType(aux,m_scale);
		}

		if (aux == "[playback]")
		{
			in >> aux;
			SetFactorType(aux,m_playback);
			in >> aux;
			SetFactorType(aux,m_playback);
		}
	}
	in.close();
}

ConfigFile::ConfigFile(const std::string& fileName)
{
}

void ConfigFile::SetFactorType(const std::string& data, factortype& scaleorplayback)
{

	if (data[0] == 'f')
	{
		
		scaleorplayback.factor = std::stoi(data.substr(7));
	}
	if (data[0] == 't')
	{
		scaleorplayback.type = data.substr(5);
	}
}

std::string ConfigFile::GetInputData() const
{
	return m_inputData;
}

std::string ConfigFile::GetOutputData() const
{
	return m_outputData;
}

std::string ConfigFile::GetProcessors() const
{
	return m_processors;
}

ConfigFile::factortype& ConfigFile::GetPlayback()
{
	return m_playback;
}

ConfigFile::factortype& ConfigFile::GetScale()
{
	return m_scale;
}
