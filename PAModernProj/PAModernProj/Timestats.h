#pragma once
#include<time.h>
#include<iostream>
#include<string>

//struct for time easier to use ;)
struct TimeStruct
    {  
       int16_t hour;
       int16_t minutes;
       int16_t seconds;
       void SetTime(const time_t& time);
       
       
    };
class Timestats
{   
private:
    /* data */
    time_t m_starttime;
    time_t m_endtime;
    TimeStruct m_diftime;
public:
        
    //convert the time into a string for the << operator   
    static std::string HmsString(const TimeStruct& time);
    //sets the convert the time_t to a timestruct
    static TimeStruct GetTimeStruct(const time_t & thetime); 
    //the << operator overload... will show the summary of the Time Statistics
    friend std::ostream& operator<<(std::ostream& os, Timestats const & tc);
    Timestats();
   // start the timer
     void StartTimer();
    //stop the timer
    void StopTimer();
    //set the diference between the times
  TimeStruct SetElapsedTime();
};

