#include <iostream>
#include"Timestats.h"
#include"ConfigFile.h"
#include"ConfigArg.h"
#include"AudioFile.h"
int main(int argc, char **argv )
{
  Timestats tst;
  tst.StartTimer();
  AudioFile audio;
  audio.load("da.wav");
  for(auto a:audio.m_data)
  {
     std::cout<<a<<" ";
  }
  tst.StopTimer();
  std::cout<<'\n'<<tst;
  tst.StartTimer();
  audio.save("danew.wav");
  tst.StopTimer();
  std::cout<<'\n'<<tst;
}