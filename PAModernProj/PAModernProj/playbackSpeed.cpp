#include "playbackSpeed.h"

std::vector<int16_t> playbackSpeed::increaseSpeed(int16_t percentage, std::vector<int16_t> sounds)
{
	for (int16_t sound : sounds)
	{
		sound = sound * (100 + percentage);
	}
	return sounds;
}

std::vector<int16_t> playbackSpeed::decreaseSpeed(int16_t percentage, std::vector<int16_t> sounds)
{
	for (int16_t sound : sounds)
	{
		sound = sound * (100 - percentage);
	}
	return sounds;
}
