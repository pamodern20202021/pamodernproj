#include"Timestats.h"
//timestruct implementation
void TimeStruct::SetTime(const time_t& time)
{
    struct tm* auxilliary;
    auxilliary=localtime(&time);
    this->hour=auxilliary->tm_hour;
    this->minutes=auxilliary->tm_min;
    this->seconds=auxilliary->tm_sec;
    
}
//timestats implementations
TimeStruct Timestats::GetTimeStruct(const time_t & thetime)
{
        TimeStruct ts;
        ts.SetTime(thetime);
        return ts;
}
std::string Timestats::HmsString(const TimeStruct& time)
{
  std::string result;
  result=std::to_string(time.hour)
+":"+std::to_string(time.minutes)
+":"+std::to_string(time.seconds);
return result;
}
 std::ostream& operator<<(std::ostream& os, Timestats const & tc)
{
std::string summary;
TimeStruct stimeinfo;


summary="<------TimeStats------>\n";
stimeinfo=Timestats::GetTimeStruct(tc.m_starttime);
summary+="Start Time: "+Timestats::HmsString(stimeinfo)+"\n";
stimeinfo=Timestats::GetTimeStruct(tc.m_endtime);
summary+="End Time: "+Timestats::HmsString(stimeinfo)+"\n";

summary+="Time consumed by this Task: "
+Timestats::HmsString(tc.m_diftime)+"\n";
summary+="<----TimeStats End---->\n";
os<<summary;
return os;
    
}

void Timestats::StartTimer()
{
    time(&m_starttime);
    m_endtime=NULL;
}
void Timestats::StopTimer()
{
    time(&m_endtime);
   m_diftime =SetElapsedTime();
}
TimeStruct Timestats::SetElapsedTime()
{
    TimeStruct result,startstruct,endstruct;
    startstruct.SetTime(m_starttime);
    endstruct.SetTime(m_endtime);
    result.hour=endstruct.hour-startstruct.hour;
    result.minutes=endstruct.minutes-startstruct.minutes;
    result.seconds=endstruct.seconds-startstruct.seconds;
   return result;
}
Timestats::Timestats()
{
    m_starttime=0;
    m_endtime=0;
}