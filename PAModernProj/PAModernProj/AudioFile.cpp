#include"AudioFile.h"


AudioFile::AudioFile()
{
	//static_assert(std::is_floating_point < T::value, "ERROR: This version of AudioFile only supports floating point sample formats");

	
	audioFileFormat = AudioFileFormat::NotLoaded;
}


AudioFile::AudioFile(std::string filePath)
	: AudioFile()
{
	load(filePath);
}
void AudioFile::AddToStorage( std::queue<int8_t>&fullData,std::array<int16_t,4>&datain)
{
	for(auto& aux:datain)
	{
		aux=(int16_t)fullData.front();
		fullData.pop();
	}
}
void AudioFile::AddToStorage( std::queue<int8_t>&fullData,std::array<int16_t,2>&datain)
{
	for(auto& aux:datain)
	{
		aux=(int16_t)fullData.front();
		fullData.pop();
	}
}
void AudioFile::AddToStorage( std::queue<int8_t>&fullData,std::vector<int16_t>&datain)
{
	
		while(!fullData.empty())
		{
			datain.push_back((int16_t)fullData.front());
			fullData.pop();
		}
}
bool AudioFile::load(std::string filePath)
{
		std::ifstream in(filePath,std::ios::binary);
	std::queue<int8_t>tempdatas;
	int8_t tempdata;
	while(!in.eof())
	{
		in>>tempdata;
		tempdatas.push(tempdata);
	}
	AddToStorage(tempdatas,m_riffChunkId);
	AddToStorage(tempdatas,m_riffChunkSize);
	AddToStorage(tempdatas,m_riffFormat);
	AddToStorage(tempdatas,m_fmtsChunckId);
	AddToStorage(tempdatas,m_fmtsChunkSize);
	AddToStorage(tempdatas,m_AudioFormat);
	AddToStorage(tempdatas,m_numChannels);
	AddToStorage(tempdatas,m_sampleRate);
	AddToStorage(tempdatas,m_byterate);
	AddToStorage(tempdatas,m_blockAlign);
	AddToStorage(tempdatas,m_bitsPerSample);
	AddToStorage(tempdatas,m_datasChunckId);
	AddToStorage(tempdatas,m_datasChunkSize);
	AddToStorage(tempdatas,m_data);
	in.close();
return true;
}
	
void AudioFile::AddToQueue( std::queue<int16_t>&fullData,std::array<int16_t,4>&datain)
	{
		for(auto& aux:datain)
	{
		fullData.push(aux);
	}
	}
void AudioFile::AddToQueue( std::queue<int16_t>&fullData,std::array<int16_t,2>&datain)
	{
		for(auto& aux:datain)
	{
		fullData.push(aux);
	}
	}
void AudioFile::AddToQueue( std::queue<int16_t>&fullData,std::vector<int16_t>&datain)
	{
		for(auto& aux:datain)
	{
		fullData.push(aux);
	}
	}


bool AudioFile::save(const std::string& filePath, AudioFileFormat format)
{
	std::ofstream out(filePath,std::ios::binary);
	std::queue<int16_t> data;
	AddToQueue(data,m_riffChunkId);
	AddToQueue(data,m_riffChunkSize);
	AddToQueue(data,m_riffFormat);
	AddToQueue(data,m_fmtsChunckId);
	AddToQueue(data,m_fmtsChunkSize);
	AddToQueue(data,m_AudioFormat);
	AddToQueue(data,m_numChannels);
	AddToQueue(data,m_sampleRate);
	AddToQueue(data,m_byterate);
	AddToQueue(data,m_blockAlign);
	AddToQueue(data,m_bitsPerSample);
	AddToQueue(data,m_datasChunckId);
	AddToQueue(data,m_datasChunkSize);
	AddToQueue(data,m_data);
	while(!data.empty())
	{
		out<<(char)data.front();
		data.pop();
	}
	out.close();
	return true;
}

AudioFileFormat AudioFile::determineAudioFileFormat(std::string filename)
{
    if(filename.substr(filename.size()-4)==".wav")
    {
        return AudioFileFormat::WAV;
    }
    else
    {
        return AudioFileFormat::Error;
    }
}