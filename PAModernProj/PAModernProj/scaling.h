#pragma once
#include<vector>
#include<iostream>
#include<vector>
class scaling
{
public:
	//modifica volumul cu un anumit procent

	
	void setVolume(std::vector<int16_t>& sounds, int16_t& vol);
	int16_t getVolume(std::vector<int16_t>& sounds, int16_t& vol);

	std::vector<int16_t> Scaling(int16_t& percentage, std::vector<int16_t> sounds);

	// mareste volumul cu un anumit procentaj
	std::vector<int16_t> upperScaling(const int16_t& limiter, int16_t percentage, std::vector<int16_t> sounds);

	//scade volumul cu un anumit procentaj
	std::vector<int16_t> lowerScaling(int16_t& percentage, std::vector<int16_t> sounds);
	//volumul minim si maxim al sunetelor din vect
	std::vector<int16_t> minimVolume(std::vector<int16_t>sounds);
	std::vector<int16_t> maximVolume(std::vector<int16_t>sounds, const int16_t& limiter);

private:
	int16_t m_minimVolume, m_maximVolume;//values in dB
	int16_t m_Volume;
	std::vector<int16_t>sounds;
};

