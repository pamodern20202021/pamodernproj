#include <iostream>
#include<time.h>
#include <string>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <PAModernProj/ConfigFile.h>
#include<PAModernProj/Timestats.h>
#include<PAModernProj/scaling.h>
#include<PAModernProj/AudioFile.h>
#include <PAModernProj/ConfigArg.h>

TEST_CASE("Test SetTime()")
{
	time_t tm = time(NULL);
	TimeStruct ts;
	ts.SetTime(tm);
	TimeStruct controlvar;
	struct tm* auxilliary;
	auxilliary = localtime(&tm);
	controlvar.hour = auxilliary->tm_hour;
	controlvar.minutes = auxilliary->tm_min;
	controlvar.seconds = auxilliary->tm_sec;
	REQUIRE(ts.hour == controlvar.hour);
	REQUIRE(ts.minutes == controlvar.minutes);
	REQUIRE(ts.seconds == controlvar.seconds);
}
TEST_CASE("Test HmsString()")
{
    TimeStruct ts;
    ts.hour=01;
    ts.minutes=3;
    ts.seconds=12;
    REQUIRE("1:3:12"==Timestats::HmsString(ts));
}

TEST_CASE("Test set factor type")
{
    std::string data;
    ConfigFile cfgf;
    data = "factor=2";
    ConfigFile::SetFactorType(data, cfgf.GetScale());
    REQUIRE(cfgf.GetScale().factor == 2);
    data = "type=down";
    cfgf.SetFactorType(data, cfgf.GetScale());
    REQUIRE(cfgf.GetScale().type == "down");
}

//scailing and volume
TEST_CASE("Test the scailing")
{
	scaling data;
	std::vector<int16_t> sounds;
	sounds.push_back(1);
	sounds.push_back(10);
	sounds.push_back(100);
	int16_t procent = 150;
	std::vector<int16_t>result = data.Scaling(procent,sounds);
	REQUIRE(result[0] == 1);
	REQUIRE(result[1] == 15);
	REQUIRE(result[2] == 150);

}
TEST_CASE("Test the upper scailing")
{
	scaling data;
	std::vector<int16_t> sounds;
	sounds.push_back(1);
	sounds.push_back(10);
	sounds.push_back(100);
	int16_t procent = 60;
	int16_t limiter = 150;
	std::vector<int16_t>result = data.upperScaling(limiter, procent, sounds);
	REQUIRE(result[0] == 61);
	REQUIRE(result[1] == 70);
	REQUIRE(result[2] == 150);

}
TEST_CASE("Test the lower scailing")
{
	scaling data;
	std::vector<int16_t> sounds;
	sounds.push_back(1);
	sounds.push_back(10);
	sounds.push_back(100);
	int16_t procent = 60;
	
	std::vector<int16_t>result = data.lowerScaling(procent, sounds);
	REQUIRE(result[0] == 0);
	REQUIRE(result[1] == 0);
	REQUIRE(result[2] == 40);

}
TEST_CASE("Test the minim volume")
{
	scaling data;
	std::vector<int16_t> sounds;
	sounds.push_back(123);
	sounds.push_back(11);
	sounds.push_back(223);
	std::vector<int16_t>result = data.minimVolume(sounds);
	REQUIRE(result[0] == 0);
	REQUIRE(result[1] == 0);
	REQUIRE(result[2] == 0);
}
TEST_CASE("Test the maxim volume")
{
	scaling data;
	std::vector<int16_t> sounds;
	int16_t limiter = 150;
	sounds.push_back(123);
	sounds.push_back(11);
	sounds.push_back(223);
	std::vector<int16_t>result = data.maximVolume(sounds, limiter);
	REQUIRE(result[0] == 150);
	REQUIRE(result[1] == 150);
	REQUIRE(result[2] == 150);
}
TEST_CASE("determineAudioFileFormat")
{
	AudioFile file;
	AudioFileFormat result=file.determineAudioFileFormat("da.wav");
	REQUIRE(result==AudioFileFormat::WAV);
	result=file.determineAudioFileFormat("da.wa");
	REQUIRE(result==AudioFileFormat::Error);
}


